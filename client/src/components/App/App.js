import React from "react";
import "./App.css";
import MessageList from "../MessageList";
import MessageForm from "../MessageForm";

function App() {
  return (
    <div className="chat">
      <MessageList />
      <MessageForm />
    </div>
  );
}

export default App;
