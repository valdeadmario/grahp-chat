import React, { useState } from "react";

import "./Header.css";

const Header = ({ changeSort, onFilter }) => {
  const [label, setLabel] = useState("");
  return (
    <div className="header d-flex">
      <div className="logo d-flex">
        <h1 className="name">GraphChat</h1>
      </div>
      <div className="d-flex params sorted-by">
        Sorted By
        <button
          className="btn btn-outline-danger"
          onClick={() => changeSort("createdAt_DESC")}
        >
          Time
        </button>
        <button
          className="btn btn-outline-warning"
          onClick={() => changeSort("countLike_ASC")}
        >
          Likes
        </button>
        <button
          className="btn btn-outline-primary"
          onClick={() => changeSort("countDislike_ASC")}
        >
          Dislikes
        </button>
        <div className="filter d-flex">
          <input
            type="text"
            value={label}
            onChange={e => setLabel(e.target.value)}
            className="form-control"
            placeholder="Filter.."
          />
          <button className="btn" onClick={() => onFilter(label)}>
            Search
          </button>
        </div>
      </div>
    </div>
  );
};

export default Header;
