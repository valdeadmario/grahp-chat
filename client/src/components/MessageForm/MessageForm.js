import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from "../../queries";

import "./MessageForm.css";

const MessageForm = () => {
  const [label, setLabel] = useState("");

  const _updateStoreAfterAddingProduct = (store, newMessage) => {
    const orderBy = "createdAt_DESC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy
      }
    });
    data.messages.messageList.unshift(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data
    });
  };
  console.log(label);
  return (
    <div className="d-flex message-form">
      <input
        type="text"
        className="form-control"
        onChange={e => setLabel(e.target.value)}
        placeholder="Send your message..."
        value={label}
      />

      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ message: label }}
        update={(store, { data: { postMessage } }) => {
          _updateStoreAfterAddingProduct(store, postMessage);
        }}
      >
        {postMutation => (
          <button
            className="send btn btn-outline-danger"
            onClick={postMutation}
          >
            Send
          </button>
        )}
      </Mutation>
    </div>
  );
};

export default MessageForm;
