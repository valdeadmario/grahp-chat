import React, { useState } from "react";
import { Mutation } from "react-apollo";

import "./ReplyForm.css";

import { MESSAGE_QUERY, POST_REPLIES_MUTATION } from "../../queries";

const ReplyForm = ({ messageId, toggleForm }) => {
  const [message, setMessage] = useState("");

  const _updateStoreAfterAddingReview = (store, newReplies, messageId) => {
    const orderBy = "createdAt_DESC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy
      }
    });
    const repliatedProduct = data.messages.messageList.find(
      item => item.id === messageId
    );
    console.log(repliatedProduct);
    repliatedProduct.replies.push(newReplies);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    toggleForm(false);
  };

  const onCancel = () => {
    setMessage("");
    toggleForm(false);
  };

  return (
    <div
      className="modal"
      style={{ display: "block" }}
      tabIndex="-1"
      role="dialog"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content" style={{ padding: "5px" }}>
          <div className="modal-header">
            <h5 className="modal-title">Reply message</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={onCancel}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <textarea
              className="form-control"
              rows="5"
              onChange={e => setMessage(e.target.value)}
              value={message}
            />
          </div>
          <div className="modal-footer">
            <button className="btn btn-secondary" onClick={onCancel}>
              Cancel
            </button>
            <Mutation
              mutation={POST_REPLIES_MUTATION}
              variables={{ messageId, text: message }}
              update={(store, { data: { postReply } }) => {
                _updateStoreAfterAddingReview(store, postReply, messageId);
              }}
            >
              {postMutation => (
                <button className="btn btn-warning" onClick={postMutation}>
                  Reply
                </button>
              )}
            </Mutation>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReplyForm;
