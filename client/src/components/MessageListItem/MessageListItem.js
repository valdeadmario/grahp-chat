import React from "react";

import "./MessageListItem.css";

const MessageListItem = ({ messageItem, onReply }) => {
  const { id, message, likes } = messageItem;
  const like = likes.filter(item => item.isLike).length;

  const dislike = likes.filter(item => !item.isLike).length;
  const renderButtons = () => (
    <React.Fragment>
      <button className="likes btn">
        <i className="fa fa-thumbs-up" />
        {like}
      </button>
      <button className="dislikes btn">
        <i className="fa fa-thumbs-down" />
        {dislike}
      </button>
    </React.Fragment>
  );

  const renderReply = () => (
    <button className="reply btn btn-outline-danger" onClick={() => onReply(id)}>Reply</button>
  );

  return (
    <li className={`message media`} key={id}>
      <div className="body">
        <div className="text">
          {message}
          {renderReply()}
        </div>
        <div className={`d-flex`}>
          <small className="ms-date d-inline">#{id}</small>
          {renderButtons()}
        </div>
      </div>
    </li>
  );
};

export default MessageListItem;
