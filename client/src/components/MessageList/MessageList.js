import React, { useState } from "react";
import { Query } from "react-apollo";
import Pagination from "../Pagination/Pagination";
import Header from "../Header";
import "./MessageList.css";
import MessageListItem from "../MessageListItem";
import { MESSAGE_QUERY, NEW_MESSAGES_SUBSCRIPTION } from "../../queries";
import ReplyForm from "../ReplyForm/ReplyForm";

const ELEMENT_OF_PAGE = 7;

const MessageList = () => {
  const [filter, setFilter] = useState("");
  const [orderBy, setOrderBy] = useState("createdAt_DESC");
  const [count, setCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [showRepliesForm, toggleForm] = useState(false);
  const [currentId, setCurrentId] = useState("");

  const countPages = Math.ceil(Number(count) / ELEMENT_OF_PAGE);

  const onPageChange = page => {
    setCurrentPage(page);
  };

  const onChangeSort = label => {
    setOrderBy(label);
  };

  const onFilter = filter => {
    setFilter(filter);
  };

  const onOpenReply = id => {
    toggleForm(true);
    setCurrentId(id);
  };

  const _subscribeToNewMessages = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGES_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;

        const exists = prev.messages.messageList.find(
          ({ id }) => id === newMessage.id
        );

        if (exists) return prev;

        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename
          }
        };
      }
    });
  };
  const skip = ELEMENT_OF_PAGE * (currentPage - 1);
  const first = ELEMENT_OF_PAGE;

  return (
    <div>
      {showRepliesForm && (
        <ReplyForm messageId={currentId} toggleForm={toggleForm} />
      )}
      <Header changeSort={onChangeSort} onFilter={onFilter} />
      <Query query={MESSAGE_QUERY} variables={{ orderBy, skip, first, filter }}>
        {({ loading, error, data, subscribeToMore }) => {
          if (loading) return <div>Loading...</div>;
          if (error) return <div>Fetch error</div>;
          _subscribeToNewMessages(subscribeToMore);

          const {
            messages: { messageList, count }
          } = data;
          setCount(count);
          return (
            <div className="message-list listview lv-message">
              {messageList.map(message => {
                const { id } = message;
                return (
                  <MessageListItem
                    messageItem={message}
                    key={id}
                    onReply={onOpenReply}
                  />
                );
              })}
            </div>
          );
        }}
      </Query>
      <Pagination
        countPages={countPages}
        currentPage={currentPage}
        changePage={onPageChange}
      />
    </div>
  );
};

export default MessageList;
