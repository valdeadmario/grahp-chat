import gql from "graphql-tag";

export const MESSAGE_QUERY = gql`
  query messageQuery(
    $orderBy: MessageOrderByInput
    $skip: Int
    $first: Int
    $filter: String
  ) {
    messages(orderBy: $orderBy, skip: $skip, first: $first, filter: $filter) {
      count
      messageList {
        id
        message
        replies {
          id
          text
        }
        likes {
          id
          isLike
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($message: String!) {
    postMessage(message: $message) {
      id
      message
      replies {
        id
        text
      }
    }
  }
`;

export const POST_REPLIES_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postReply(messageId: $messageId, text: $text) {
      id
      text
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      message
      replies {
        id
        text
      }
      likes {
        id
        isLike
      }
    }
  }
`;
