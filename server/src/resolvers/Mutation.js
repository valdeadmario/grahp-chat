function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    message: args.message
  });
}

async function postReply(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.createReply({
    text: args.text,
    message: { connect: { id: args.messageId } }
  });
}

async function postLike(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.createLike({
    isLike: args.isLike,
    message: { connect: { id: args.messageId } }
  });
}

module.exports = {
  postMessage,
  postReply,
  postLike
};
