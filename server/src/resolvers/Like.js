function message(parent, args, context) {
    return context.prisma.like({
        id: parent.id
    }).message();
}

module.exports = {
    message
}